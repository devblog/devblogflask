from functools import wraps
from typing import Optional

import click
from flask import Flask, current_app
from flask_jwt_extended import (
    JWTManager,
    get_jwt_claims,
    verify_jwt_in_request,
    verify_jwt_in_request_optional,
)
from flask_jwt_extended.exceptions import UserClaimsVerificationError
from jwt import ExpiredSignatureError
from sqlalchemy import select
from sqlalchemy.orm import joinedload

from devblog.api_security.models import Role, User
from devblog.api_security.views import SECURITY_API
from devblog.db import Session
from devblog.responses import Forbidden


class FlaskTokens:
    def __init__(self, app=None):
        if app:
            self.register_app(app)

    def register_app(self, app: Flask):
        jwt = JWTManager(app)
        app.register_blueprint(SECURITY_API)

        @jwt.user_claims_loader
        def _add_claims_to_access_token(user):
            return {"roles": [role.name for role in user.roles]}

        @jwt.user_identity_loader
        def _user_identity_lookup(user):
            return user.email

        @jwt.user_loader_callback_loader
        def _user_loader_callback(identity):
            q = select(User).where(User.email == identity)
            return Session().execute(q).scalar()

        app.flask_tokens = self

        @app.cli.command("create-user")
        @click.argument("alias")
        @click.argument("email")
        @click.argument("password")
        def _create_user(alias, email, password):
            q = select(Role).where(Role.name == "admin")
            role = Session().execute(q).scalar() or Role(name="admin")
            user = User(alias=alias, email=email, password=password, roles=[role])
            Session().add_all([role, user])


def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()
        if "admin" not in claims["roles"]:
            raise Forbidden("Not authorized for this endpoint")
        return fn(*args, **kwargs)

    return wrapper


def load_token_if_current(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        try:
            verify_jwt_in_request_optional()
        except (ExpiredSignatureError, UserClaimsVerificationError):
            pass
        return fn(*args, **kwargs)

    return wrapper
