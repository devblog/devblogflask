import boto3
from botocore.client import BaseClient
from flask import Flask

from devblog.posts.views import POST_API


class DevBlog:
    _s3: BaseClient

    def __init__(self, app: Flask):
        app.register_blueprint(POST_API, url_prefix="/api/blog")
        app.devblog = self
        self.app = app

    @property
    def s3(self):
        # This is mocked out in testing
        if not hasattr(self, "_s3"):  # pragma: no cover
            self._s3 = boto3.client("s3")
        return self._s3
