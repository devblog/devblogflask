# pylint: disable=redefined-outer-name,protected-access
import os
from pathlib import Path

import boto3
import pytest
from botocore.stub import Stubber
from sqlalchemy import event

import alembic.config
from devblog.application import create_app
from devblog.db import Session
from tests.factories import AdminUser


@pytest.fixture(scope="session")
def _migrations():
    folder = Path(__file__).parent.parent
    os.chdir(folder)
    ini = folder.joinpath("alembic.ini")
    _app = create_app()
    args = ["--raiseerr", "-c", str(ini)]

    alembic.config.main(argv=args + ["upgrade", "head"])
    yield
    alembicArgs = args + ["downgrade", "base"]
    alembic.config.main(argv=alembicArgs)


@pytest.fixture
def _s3(_app):
    c = boto3.client("s3")
    stub = Stubber(c)
    stub.activate()
    _app.devblog._s3 = c
    yield stub
    del _app.devblog._s3


@pytest.fixture(name="_app")
def app_fixture():
    app = create_app()
    app.config["TESTING"] = True
    app.config["BCRYPT_ROUNDS"] = 4
    with app.test_request_context():
        yield app


@pytest.fixture
def client(_app):
    with _app.test_client() as c:
        yield c


@pytest.fixture
def _session(_app, _migrations):
    with Session.session_factory.engine.connect() as conn:
        with conn.begin() as transaction:
            session = Session(bind=conn)
            session.begin_nested()

            @event.listens_for(session, "after_transaction_end")
            def end_savepoint(_sess, _trans):
                if not _sess.in_nested_transaction():
                    _sess.begin_nested()

            yield session
            transaction.rollback()


def login(email, client):
    json = {"email": email, "password": "somepassword"}
    response = client.post("/api/login", json=json)
    assert response.status_code == 200
    token = response.json["data"]["access_token"]
    return {"Authorization": f"Bearer {token}"}


@pytest.fixture(name="_user")
def user(_session):
    _user = AdminUser()
    _session.add(_user)
    _session.commit()
    return _user


@pytest.fixture(name="_logged_in")
def logged_in(client, _user):
    return login(_user.email, client)
