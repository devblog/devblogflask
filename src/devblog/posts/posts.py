from flask import request
from flask_jwt_extended import get_jwt_claims, get_jwt_identity
from marshmallow import EXCLUDE, Schema, fields
from sqlalchemy import delete, select

from devblog.api_security import User
from devblog.db import Session
from devblog.posts.images import ImageSchema
from devblog.posts.models import Image, Post
from devblog.responses import Forbidden, NotFound


def get_post_query():
    q = select(Post).order_by(Post.timestamp.desc())
    if not request.args.get("drafts"):
        return q.where(Post.published.is_(True))

    claims = get_jwt_claims()
    if claims and "admin" in claims.get("roles"):
        return q.where(Post.published.is_(False))

    current_user = get_jwt_identity()
    if current_user:
        return (
            q.join(User)
            .where(Post.published.is_(False))
            .where(User.email == current_user)
        )
    raise Forbidden("You cannot view drafts without logging in")


def get_post_list():
    return Session().execute(get_post_query()).scalars()


def get_post_detail(slug):
    if request.method == "DELETE":
        q = delete(Post).where(Post.slug == slug)
        Session().execute(q)
        return None

    q = select(Post).where(Post.slug == slug).with_for_update()
    post_model = Session().execute(q).scalar()
    if not post_model:
        raise NotFound(f"No Post with slug {slug}")
    check_post_permissions(post_model)
    if request.method == "GET":
        return post_model

    post_data = PostDetailSchema().load(request.json["post"], unknown=EXCLUDE)
    image = post_data.pop("header_image", {})
    images = post_data.pop("images", [])
    if image:
        post_data["header_image_id"] = image["id"]
    if images:
        new_images = [Session().get(Image, img["id"]) for img in images]
        post_model.images = new_images
    for key, value in post_data.items():
        setattr(post_model, key, value)
    Session().commit()
    Session().refresh(post_model)
    return post_model


def check_post_permissions(post):
    current_user = get_jwt_identity()
    roles = get_jwt_claims()
    if not any(
        (
            post.published,
            current_user and "admin" in roles,
            post.author and post.author.email == current_user,
        )
    ):
        raise Forbidden("You don't have permission to view this post")


class AuthorSchema(Schema):
    id = fields.Integer()
    alias = fields.Str()


class PostStubSchema(Schema):
    id = fields.Integer()
    title = fields.Str()
    slug = fields.Str(dump_only=True)
    tagline = fields.Str()
    published = fields.Boolean()
    timestamp = fields.DateTime(dump_only=True)
    header_image = fields.Nested(ImageSchema, missing=None, unknown=EXCLUDE)
    author = fields.Nested(AuthorSchema, dump_only=True, unknown=EXCLUDE)


class PostDetailSchema(PostStubSchema):
    content = fields.Str()
    images = fields.List(fields.Nested(ImageSchema))
