from bcrypt import checkpw
from flask import Blueprint, request
from flask_jwt_extended import create_access_token, get_current_user, jwt_required
from marshmallow import EXCLUDE, Schema, ValidationError, fields, post_load
from pockets.autolog import log
from sqlalchemy import select

from devblog.api_security.models import User
from devblog.db import Session
from devblog.responses import ApiError, Unauthorized, enforce_json, make_response

SECURITY_API = Blueprint("security_api", __name__)


@enforce_json
def check_login():
    email = request.json.get("email", None)
    log.debug(f"Logging in: {email}")
    password = request.json.get("password", None)
    if not email:
        raise ApiError("Missing username")
    if not password:
        raise ApiError("Missing password")

    q = select(User).where(User.email == email)
    user = Session().execute(q).scalar()
    log.debug(f"Found user- alias: {user and user.alias}")
    if not user or not checkpw(password.encode("utf-8"), user.password.encode("utf-8")):
        raise Unauthorized("Bad username or password")
    return user


@SECURITY_API.route("/api/login", methods=["POST"])
def login():
    user = check_login()
    access_token = create_access_token(identity=user, fresh=True)
    return make_response(access_token=access_token)


class RegisterSchema(Schema):
    alias = fields.Str(required=True)
    email = fields.Email(required=True)
    password = fields.Str(required=True)
    active = fields.Bool()
    confirmed_at = fields.DateTime()

    @post_load
    def make_user(self, data, **kwargs):  # pylint: disable=no-self-use
        return User(**data)


@SECURITY_API.route("/api/register", methods=["POST"])
@enforce_json
def register():
    """View function which handles a registration request."""
    try:
        schema = RegisterSchema()
        Session().add(schema.load(request.json))
        user = check_login()
        access_token = create_access_token(identity=user, fresh=True)
        return make_response(access_token=access_token)
    except ValidationError as e:
        raise ApiError(e.messages) from e


class UserSchema(Schema):
    alias = fields.Str()
    email = fields.Email()
    active = fields.Bool()
    confirmed_at = fields.DateTime()

    class Meta:
        unknown = EXCLUDE


@SECURITY_API.route("/api/user")
@jwt_required
def get_user_data():
    current_user = get_current_user()
    schema = UserSchema()
    return make_response(user=schema.dump(current_user))
