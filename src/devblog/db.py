from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm import declarative_base, scoped_session, sessionmaker

from devblog import config


class SessionFactory:
    _engine: Engine
    _sessionmaker: sessionmaker

    @property
    def engine(self):
        if not hasattr(self, "_engine"):
            self._engine = create_engine(config.SQLALCHEMY_DATABASE_URI)
        return self._engine

    @property
    def sessionmaker(self):
        if not hasattr(self, "_sessionmaker"):
            self._sessionmaker = sessionmaker(bind=self.engine)
        return self._sessionmaker

    def __call__(self, *args, **kwargs):
        return self.sessionmaker(*args, **kwargs)


Session = scoped_session(SessionFactory())
Base = declarative_base()
