from flask import Flask, session

from devblog.responses import ApiException, ApiExceptions, make_response


def test_cache_control(client):
    response = client.get("/health")
    assert response.status_code == 200
    assert response.headers["Cache-Control"] == "no-cache"


def test_session_control(client):
    response = client.get("/health")
    assert response.status_code == 200
    assert "id" in session


def test_health_check(client):
    response = client.get("/health")
    assert response.status_code == 200
    assert response.json["data"]["health_check"] == "OK"


def test_build_exception():
    try:
        raise ApiException("my message", 499)
    except ApiException as e:
        assert e.message == "my message"
        assert e.status_code == 499


def test_default_exception():
    try:
        raise ApiException()
    except ApiException as e:
        assert e.message == "an error occurred"
        assert e.status_code == 500


def test_serialize_exception(_app):
    response, code = make_response(error=ApiException("my message", 499))
    assert response.json["error"]["message"] == "my message"
    assert response.json["error"]["type"] == "Error"
    assert code == 499


def test_response(_app):
    response, code = make_response(mydata=35)
    assert response.json["data"]["mydata"] == 35
    assert code == 200


def test_catches_uncaught_exception():
    app = Flask(__name__)
    ApiExceptions(app)

    @app.route("/")
    def _root():
        raise Exception()

    with app.test_client() as client:
        response = client.get("/")

    assert response.status_code == 500
    assert response.json["error"]["message"] == "uncaught exception"


def test_late_register():
    api = ApiExceptions()
    app = Flask(__name__)
    api.register_app(app)

    @app.route("/")
    def _root():
        raise Exception()

    with app.test_client() as client:
        response = client.get("/")

    assert response.status_code == 500
    assert response.json["error"]["message"] == "uncaught exception"
