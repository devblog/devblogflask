import os
from datetime import timedelta
from pathlib import Path

_host = os.environ.get("POSTGRES_HOST", "localhost")
_pw = os.environ.get("POSTGRES_PASSWORD", "random_password").strip()
SQLALCHEMY_DATABASE_URI = f"postgresql://postgres:{_pw}@{_host}:5432/postgres"
# Need to make a secure one of these if we start relying on the session being
# secure
SECRET_KEY = "NOTSECUREKEY"

JWT_ACCESS_TOKEN_EXPIRES = timedelta(days=30)
JWT_ALGORITHM = "RS512"
key_path = os.environ.get("KEY_PATH", "insecure_keys")
JWT_PUBLIC_KEY = (Path(key_path) / "public_key.pem").read_text()
JWT_PRIVATE_KEY = (Path(key_path) / "private_key.pem").read_text()
BCRYPT_ROUNDS = 5

S3_BUCKET = "paulbecotte-images"
S3_UPLOAD_DIRECTORY = "img"

ELASTIC_APM = {
    "SERVICE_NAME": "devblog-flask",
    "SERVER_URL": "http://elastic-apm-server.elastic:8200",
}
