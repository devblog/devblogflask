from datetime import datetime

from bcrypt import gensalt, hashpw
from flask import current_app
from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String, Table
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import backref, relationship

from devblog.db import Base

roles_user = Table(
    "roles_user",
    Base.metadata,
    Column("user_id", Integer, ForeignKey("user.id"), primary_key=True),
    Column("role_id", Integer, ForeignKey("role.id"), primary_key=True),
)


class Role(Base):
    __tablename__ = "role"
    id = Column(Integer, primary_key=True)
    name = Column(String(length=80), unique=True)
    description = Column(String(length=255))


class User(Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True)
    alias = Column(String(255))
    email = Column(String(255))
    hashed_password = Column("password", String(255))
    active = Column(Boolean(), default=True)
    confirmed_at = Column(DateTime(), default=datetime.utcnow)
    roles: relationship = relationship(
        Role, secondary=roles_user, backref=backref("users", lazy="dynamic")
    )

    @hybrid_property
    def password(self):
        return self.hashed_password

    @password.setter  # type: ignore
    def password(self, value):
        rounds = current_app.config.get("BCRYPT_ROUNDS") if current_app else 4
        if not isinstance(value, bytes):
            value = value.encode("utf-8")
        self.hashed_password = hashpw(value, gensalt(rounds)).decode("utf-8")
