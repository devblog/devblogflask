from datetime import datetime, timedelta
from logging.config import fileConfig
import os
import sys
import time

from alembic import context
from sqlalchemy import engine_from_config, pool
from sqlalchemy.exc import OperationalError

from devblog.db import Base

sys.path.append(os.getcwd())

from devblog.application import create_app


# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config
# configure logging
fileConfig(config.config_file_name)
# add your model's MetaData object here
# for 'autogenerate' support
app = create_app()
config.set_main_option("sqlalchemy.url", app.config["SQLALCHEMY_DATABASE_URI"])

target_metadata = Base.metadata


def run_migrations_online():
    alembic_config = config.get_section(config.config_ini_section)

    engine = engine_from_config(
        alembic_config,
        prefix='sqlalchemy.',
        poolclass=pool.NullPool,
    )

    connection = None
    start_time = datetime.now()
    while connection is None:
        # Retry the connection for 60 seconds to prevent race conditions
        try:
            connection = engine.connect()
        except OperationalError as e:
            if datetime.now() > start_time + timedelta(seconds=60):
                print("Timed out connecting to database")
                raise e
            time.sleep(1)

    context.configure(
        connection=connection,
        target_metadata=target_metadata
    )

    try:
        with context.begin_transaction():
            context.run_migrations()
    finally:
        connection.close()


run_migrations_online()
