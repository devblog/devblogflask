import re
from datetime import datetime

from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String, Text
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship

from devblog.db import Base


class Post(Base):
    __tablename__ = "post"
    id = Column(Integer, primary_key=True)
    author_id = Column(Integer, ForeignKey("user.id"))
    author: relationship = relationship("User")
    _title = Column("title", String(255), unique=True)
    _slug = Column("slug", String(255), unique=True)
    tagline = Column(String(5000))
    content = Column(Text(50000))
    published = Column(Boolean())
    timestamp = Column(DateTime(), default=datetime.now)
    header_image_id = Column(Integer, ForeignKey("image.id"))
    header_image: relationship = relationship("Image", foreign_keys=[header_image_id])
    images: relationship = relationship("Image", foreign_keys="Image.post_id")
    comments: relationship = relationship(
        "Comment", order_by="Comment.timestamp", back_populates="post"
    )

    @hybrid_property
    def title(self):
        return self._title

    @title.setter  # type: ignore
    def title(self, value):
        if not self.slug:
            self._slug = re.sub(r"[^\w]+", "-", value.lower())
        self._title = value

    @hybrid_property
    def slug(self):
        return self._slug

    @slug.setter  # type: ignore
    def slug(self, value):  # pylint: disable=no-self-use
        raise AttributeError("This property is read only")


class Image(Base):
    __tablename__ = "image"
    id = Column(Integer, primary_key=True)
    url = Column(String(255), nullable=False)
    post_id = Column(Integer, ForeignKey("post.id"), nullable=True)
    post: relationship = relationship(
        "Post", foreign_keys=post_id, back_populates="images"
    )


class Comment(Base):
    __tablename__ = "comment"
    id = Column("id", Integer(), primary_key=True)
    user_id = Column("user_id", Integer(), ForeignKey("user.id"), nullable=False)
    user: relationship = relationship("User")
    post_id = Column("post_id", Integer(), ForeignKey("post.id"), nullable=False)
    post: relationship = relationship("Post", back_populates="comments")
    text = Column("text", Text(), nullable=True)
    timestamp = Column("timestamp", DateTime(), nullable=False)
