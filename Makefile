typing:
	mypy src/devblog --ignore-missing-imports

format:
	isort -rc -tc src/devblog tests
	black src/devblog tests

check-format:
	isort -rc -tc --check-only src/devblog tests
	black --check src/devblog tests

lint:
	pylint src/devblog tests

unit-test:
	pytest --no-cov tests

test: check-format lint typing
	pytest tests

docker-build:
	docker build -t local/devblogflask:latest .

docker-test: docker-build
	docker-compose -p TEST run --rm blog make test

docker-unit-test: format
	docker-compose -p TEST run --rm blog make unit-test

migrate:
	alembic upgrade head

docker-migrate:
	docker-compose run --rm blog make migrate

down:
	docker-compose down -v

up:
	docker-compose up -d

restore:
	docker-compose run -v `pwd`:/tmp -e PGPASSWORD=random_password postgres psql -U postgres -h postgres -f /tmp/backup.sql postgres

create-user:
	docker-compose run --rm blog flask create-user "$${USER}" "$${EMAIL}" "$${PASS}"
