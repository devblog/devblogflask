FROM python:3.9
WORKDIR /app

RUN groupadd -g 10100 uwsgi && useradd -g uwsgi  -u 10100 uwsgi

RUN mkdir -p /root/.config/pypoetry && touch /root/.config/pypoetry/config.toml
RUN pip install poetry --pre
COPY pyproject.toml poetry.lock /app/
RUN poetry config virtualenvs.create false
RUN poetry install --no-root

COPY . /app/
ENV PYTHONPATH=/app/src
ENV FLASK_APP=devblog.uwsgi:app
ENV KEY+PATH=/keys
CMD ["uwsgi", "--die-on-term", "--ini", "/app/conf/uwsgi/blog.ini"]
