from datetime import datetime, timedelta

import factory

from devblog.api_security.models import Role, User
from devblog.posts.models import Comment, Image, Post


class RoleFactory(factory.Factory):
    class Meta:
        model = Role

    name = factory.Sequence(lambda n: f"Role{n}")


class UserFactory(factory.Factory):
    class Meta:
        model = User

    alias = factory.Sequence(lambda n: f"somename{n}")
    email = factory.Sequence(lambda n: f"user{n}@example.fake")
    password = "somepassword"


class AdminUser(UserFactory):
    roles = factory.List([factory.SubFactory(RoleFactory, name="admin")])


class PostFactory(factory.Factory):
    class Meta:
        model = Post

    author = factory.SubFactory(AdminUser)
    title = factory.Sequence(lambda n: f"Article {n}")
    tagline = factory.Sequence(lambda n: f"Tagline {n}")
    content = factory.Sequence(lambda n: f"Lorem Ipsum {n}")
    published = True
    timestamp = datetime.now() - timedelta(days=2)


class ImageFactory(factory.Factory):
    class Meta:
        model = Image

    id: int
    url = factory.Sequence(lambda n: f"/image/url/{n}")


class CommentFactory(factory.Factory):
    class Meta:
        model = Comment

    id: int
    user: factory.SubFactory(UserFactory)
    post: factory.SubFactory(PostFactory)
    text = "This is some text for a comment"
    timestamp = datetime.now() - timedelta(days=2)
