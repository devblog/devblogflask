import os
from uuid import uuid4

from flask import Flask, Response, session
from flask_cors import CORS
from pockets.autolog import log
from prometheus_flask_exporter.multiprocess import UWsgiPrometheusMetrics

from devblog.api_security import FlaskTokens
from devblog.db import Session
from devblog.posts import DevBlog
from devblog.responses import ApiExceptions, make_response


def create_app():
    app = Flask(__name__)
    app.config.from_object("devblog.config")
    log.info("Initializing application")
    log.info(f"LOG LEVEL {log.level}")
    CORS(app)
    FlaskTokens(app)
    ApiExceptions(app)
    DevBlog(app)
    if os.environ.get("ENV") == "release":
        metrics = UWsgiPrometheusMetrics(app)
        metrics.start_http_server(9100)

    @app.route("/health")
    def _health():
        return make_response(health_check="OK")

    @app.after_request
    def _cache_control(response: Response):
        response.headers["Cache-Control"] = "no-cache"
        if "id" not in session:
            session["id"] = uuid4()
        return response

    @app.teardown_appcontext
    def shutdown_session(response_or_exc):
        Session.remove()
        return response_or_exc

    return app
